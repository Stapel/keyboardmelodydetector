#include <Arduino.h>
#include "tone_pin_mapping.h"
#include "melody.h"

#define KEY_PRESSED_COUNTER   (255U)
#define NO_KEY_PRESSED        (255U)
#define THRESHOLD             (700U)
#define NUMBER_OF_KEYS        (8U)
#define DEBUGGING_ENABLED     (1U)
#define NUMBER_OF_RELAYS      (2U)
#define RESET_KEY             (TONE_FIS0)
#define RESET_KEY_DURATION    (30000U)

typedef enum {
  IDLE_STATE,
  CORRECT_TONE_1,
  CORRECT_TONE_2,
  CORRECT_TONE_3,
  CORRECT_TONE_4,
  CORRECT_TONE_5,
  CORRECT_TONE_6,
  CORRECT_TONE_7,
  CORRECT_TONE_8,
  INCORRECT_TONE,
  RESET_OUTPUT,
  ERROR_STATE
} e_fsm_states;

typedef struct {
  uint8_t u8_tone;
  uint16_t u16_state_timeout;
} s_conf_melody;

uint16_t key_pressed_array[NUMBER_OF_KEYS] = {0,0,0,0,0,0,0,0};
uint8_t pin_array[NUMBER_OF_KEYS] = {A0, A1, A2, A3, A4, A5, A6, A7};
uint8_t relay_pins[NUMBER_OF_RELAYS] = {PIN2, PIN3};
uint8_t last_pressed_key = NO_KEY_PRESSED;
s_conf_melody as_melody[NUM_OF_MELODY_TONES] = MELODY;
e_fsm_states e_current_fsm_state = IDLE_STATE;
uint32_t reset_key_duration = 0;
uint32_t state_timeout_counter = 0;
bool output_active = false;

void setup()
{
  for(uint8_t i = 0; i < NUMBER_OF_RELAYS; ++i)
  {
    pinMode(relay_pins[i], OUTPUT);
  }
  Serial.begin(115200);
}

void setRelay(uint8_t pinIdx, bool setOn)
{
  if(pinIdx >= NUMBER_OF_RELAYS)
  {
    // TODO: ERROR
    return;
  }
  else
  {
    if(setOn)
    {
      digitalWrite(relay_pins[pinIdx], HIGH);
    }
    else
    {
      digitalWrite(relay_pins[pinIdx], LOW);      
    }
  }
}

void detect_keys()
{
  uint16_t u16_readVal[NUMBER_OF_KEYS];
  uint8_t num_pressed_keys = 0;

  for(uint8_t i = 0; i < NUMBER_OF_KEYS; ++i)
  {
    u16_readVal[i] = analogRead(pin_array[i]);
    if(u16_readVal[i] > THRESHOLD)
    {
      key_pressed_array[i] = KEY_PRESSED_COUNTER;
      last_pressed_key = i;
    }

    if(key_pressed_array[i] > 0)
    {
      num_pressed_keys++;
      key_pressed_array[i]--;
    }
  }

  if(num_pressed_keys == 0)
  {
    last_pressed_key = NO_KEY_PRESSED;
  }
#if DEBUGGING_ENABLED
  if(last_pressed_key != NO_KEY_PRESSED)
  {
    Serial.println(last_pressed_key);
  }
#endif
}

e_fsm_states checkTone(uint8_t current_tone_idx, e_fsm_states current_state, e_fsm_states success_next_state, e_fsm_states fail_next_state)
{
  static bool correct_key_pressed = false;
  e_fsm_states e_return = current_state;

  state_timeout_counter++;
  if(last_pressed_key == as_melody[current_tone_idx].u8_tone)
  {
    correct_key_pressed = true;
  }
  else if(last_pressed_key == NO_KEY_PRESSED && correct_key_pressed)
  {
    correct_key_pressed = false;
    state_timeout_counter = 0;
    e_return = success_next_state;
  }
  else if(last_pressed_key != NO_KEY_PRESSED)
  {
    correct_key_pressed = false;
    e_return = fail_next_state;
  }

  if(state_timeout_counter > as_melody[current_tone_idx].u16_state_timeout)
  {
    e_current_fsm_state = IDLE_STATE;
  }

  return e_return;
}

void loop()
{
  detect_keys();

  switch(e_current_fsm_state)
  {
    case IDLE_STATE:
    {
      state_timeout_counter = 0;
      e_current_fsm_state = checkTone(0, IDLE_STATE, CORRECT_TONE_1, IDLE_STATE);
      break;
    }
    case CORRECT_TONE_1:
    {
      e_current_fsm_state = checkTone(1, CORRECT_TONE_1, CORRECT_TONE_2, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_2:
    {
      e_current_fsm_state = checkTone(2, CORRECT_TONE_2, CORRECT_TONE_3, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_3:
    {
      e_current_fsm_state = checkTone(3, CORRECT_TONE_3, CORRECT_TONE_4, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_4:
    {
      state_timeout_counter++;
      e_current_fsm_state = checkTone(4, CORRECT_TONE_4, CORRECT_TONE_5, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_5:
    {
      e_current_fsm_state = checkTone(5, CORRECT_TONE_5, CORRECT_TONE_6, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_6:
    {
      e_current_fsm_state = checkTone(6, CORRECT_TONE_6, CORRECT_TONE_7, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_7:
    {
      e_current_fsm_state = checkTone(7, CORRECT_TONE_7, CORRECT_TONE_8, INCORRECT_TONE);
      break;
    }
    case CORRECT_TONE_8:
    {
      setRelay(0, true);
      setRelay(1, true);
      output_active = true;
      e_current_fsm_state = IDLE_STATE;
      break;
    }
    case INCORRECT_TONE:
    {
      e_current_fsm_state = IDLE_STATE;
      break;
    }
    case RESET_OUTPUT:
    {
      setRelay(0, false);
      setRelay(1, false);
      reset_key_duration = 0;
      output_active = false;
      e_current_fsm_state = IDLE_STATE;
      break;
    }
    case ERROR_STATE:
    {
      break;
    }
  }

  if (e_current_fsm_state != ERROR_STATE)
  {
    if(output_active)
    {
      reset_key_duration++;
      if (reset_key_duration > RESET_KEY_DURATION)
      {
        e_current_fsm_state = RESET_OUTPUT;
      }
    }
  }
}
